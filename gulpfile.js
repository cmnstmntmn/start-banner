var gulp = require('gulp');
var gulpif = require('gulp-if');
var sprity = require('sprity');
var minifyHTML = require('gulp-minify-html');
 
// generate sprite.png and _sprite.scss 
gulp.task('sprites', function () {
  return sprity.src({
    src: './src/img/**/*.{png,jpg}',
    style: './sprite.css',
    split: true,
    cssPath: '../img',
    prefix: 'olx',
	'dimension': [{
	  ratio: 1, dpi: 72
	}, {
	  ratio: 2, dpi: 192
	}],
    // ... other optional options 
    // for example if you want to generate scss instead of css 
    processor: 'css', // make sure you have installed sprity-sass 
  })
  .pipe(gulpif('*.png', gulp.dest('./dist/img/'), gulp.dest('./dist/css/')))
});

/* Minify HTML */
gulp.task('minify-html', function () {
  var opts = {
    conditionals: true,
    comments: false,
    spare: true,
    loose: true
  };

  return gulp.src('./src/*.html')
    .pipe(minifyHTML(opts))
    .pipe(gulp.dest('./dist'));
});


// Watch
gulp.task('watch', function () {
  gulp.watch('./src/*.html', ['minify-html']);
});

gulp.task('default', ['watch']);